<?php

namespace App\GraphQL\Query;

use App\Product;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class ProductsQuery extends Query
{
    protected $attributes = [
        'name' => 'Products Query',
        'description' => 'A query of product'
    ];

    public function type()
    {
        return GraphQL::paginate('products');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::string()
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string()
            ],
            'user_id' => [
                'name' => 'user_id',
                'type' => Type::int()
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int()
            ],
            'page' => [
                'name' => 'page',
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields)
    {
        $limit = $args['limit'] ?? 10;
        $page = $args['page'] ?? 1;
        $builder = Product::with(array_keys($fields->getRelations()));

        if (isset($args['id'])) {
            $builder->where('id', $args['id']);
        }

        if (isset($args['title'])) {
            $builder->where('title', 'like', $args['title']);
        }

        if (isset($args['user_id'])) {
            $builder->where('user_id', $args['user_id']);
        }

        return $builder->select($fields->getSelect())->paginate($limit, ['*'], 'page', $page);
    }
}
