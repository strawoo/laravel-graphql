<?php

namespace App\GraphQL\Query;

use App\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class UsersQuery extends Query
{
    protected $attributes = [
        'name' => 'Users Query',
        'description' => 'A query of users'
    ];

    public function type()
    {
        return GraphQL::paginate('users');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'email' => [
                'name' => 'email',
                'type' => Type::string()
            ],
            'limit' => [
                'name' => 'limit',
                'type' => Type::int()
            ],
            'page' => [
                'name' => 'page',
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, SelectFields $fields)
    {
        $limit = $args['limit'] ?? 10;
        $page = $args['page'] ?? 1;
        $builder = User::with(array_keys($fields->getRelations()));

        if (isset($args['id'])) {
            $builder->where('id', $args['id']);
        }

        if (isset($args['email'])) {
            $builder->where('email', $args['email']);
        }

        return $builder->select($fields->getSelect())->paginate($limit, ['*'], 'page', $page);
    }
}
